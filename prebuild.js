// load dependencies
var Handlebars = require('handlebars');
var fs = require('fs');

// prepare template
var source = fs.readFileSync('template.html', "utf8");
var template = Handlebars.compile(source);

// read data
var data = require('./data.json');

// populate template
var md = template({ data: data });

var dir = './innehall';
if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}
// write to file
fs.writeFileSync(dir + "/index.md", md, 'utf8');
